package AOC.AOC_V1.controlleur;

public interface Controlleur {
	
	public void start();
	public void stop();
	public void inc();
	public void dec();
	
	public void updateTempo(int tempo);
	public void updateMesure(int mesure);
	public void updateEnMarche(boolean enMarche);
}
