package AOC.AOC_V1.controlleur;
import AOC.AOC_V1.moteur.Moteur;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ControlleurImpl implements Controlleur {

	private Moteur moteur = null;
	private Boolean enMarche = false;
	private int tempo = 120;
	private int mesure = 4; // tout les n tempo
	
	@FXML
	private Button start;
	@FXML
	private Button stop;
	@FXML
	private Button inc;
	@FXML
	private Button dec;

	public void start(){
		start.setOnAction(null);
		moteur.setEnMarche(true);
	}

	public void stop(){
		stop.setOnAction(null);
		moteur.setEnMarche(false);
	}

	public void inc(){
		inc.setOnAction(null);
	}

	public void dec(){
		dec.setOnAction(null);
	}

	public void updateTempo(int tempo) {
		moteur.setTempo(tempo);

	}

	public void updateMesure(int mesure) {
		moteur.setMesure(mesure);

	}

	public void updateEnMarche(boolean enMarche) {
		moteur.setEnMarche(enMarche);

	}

	void init (Moteur moteur){
		this.moteur = moteur;
		
		moteur.setCommandeEnMarche(() -> {
			this.enMarche = moteur.getEnMarche();
		});
		
		moteur.setCommandeMesure(() -> {
			this.mesure = moteur.getMesure();
		});
		
		moteur.setCommandeTempo(() -> {
			this.tempo = moteur.getTempo();
		});
		
		moteur.setCommandeMarquerTempo(() -> {
			//runLater permet de demander à javafx de run les commandes dasn le bon thread
			Platform.runLater(() -> {
				// faire un truc sur l'interface
				this.Voyant1.clignote1fois();
			});
		});
		
		moteur.setCommandeMarquerMesure(() -> {
			Platform.runLater(() -> {
				// faire un autre truc sur l'interface
				this.Voyant2.clignote1fois();
			});
		});
	}
	
	
	
}
