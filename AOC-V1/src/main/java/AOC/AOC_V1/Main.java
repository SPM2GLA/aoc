package AOC.AOC_V1;

import AOC.AOC_V1.controlleur.Controlleur;
import AOC.AOC_V1.moteur.Moteur;
import AOC.AOC_V1.moteur.MoteurImpl;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("ui/UI.fxml"));
    	Parent parent = loader.load();
        //Parent root = FXMLLoader.load(getClass().getResource("UI.fxml"));
    	
    	Controlleur ctrl = loader.getController();
    	Moteur moteur = new MoteurImpl();
    	//give commands to ctrl
    	// ...
    	
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(parent, 500,500));
        primaryStage.setTitle("Métronome");
        primaryStage.show();
        
        //error : Not an Fx application thread ...   currentThread - clock-0
        //     pas dans le thread qui a lancer l'application
        //        Platform.runLater(()->{
        //        	votreLabel.setText("foo");
        //        });
    }


    public static void main(String[] args) {
        launch(args);
        //TODO: faire le lien avec l'interface, rajouter 2 voyants, faire des tests sur le moteur et le controlleur
        
        // final Engine eng = new EngineImplementation();
        // ctrl.setStartCommand(() -> {
        // eng.start();
        // }
        
        
        /* setCommand(Command c)
         * test(){
         * Controller c = new Controller();
         * Command sc = () -> {};
         * c.setStartCmd(sc);
         * verify(sc,times(1).execute();
         * }
         */
    }
}
