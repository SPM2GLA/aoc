package AOC.AOC_V1.moteur;

import AOC.AOC_V1.commande.Commande;

public interface Moteur {
	//getter et setter
	public boolean getEnMarche();
	public void setEnMarche(boolean enmarche);
	public int getTempo();
	public void setTempo(int tempo);
	public int getMesure();
	public void setMesure(int mesure);
	
	//methode set commandes
	public void setCommandeTempo(Commande c);
	public void setCommandeMesure(Commande c);
	public void setCommandeEnMarche(Commande c);
	public void setCommandeMarquerMesure(Commande c);
	public void setCommandeMarquerTempo(Commande c);
}