package AOC.AOC_V1.moteur;

import static java.util.concurrent.TimeUnit.*;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import AOC.AOC_V1.commande.Commande;


class Clock {
	private ScheduledExecutorService scheduler;
	private Commande cmd;
	
	public Clock(Commande cmd) {
		this.cmd = cmd;
	}
	//perio, temps en millisecondes entre 2 appel de la commande
	public void start(long period) { 
		if (scheduler != null) { // si il y a deja un thread running on le stop
			this.stop();
			this.scheduler = Executors.newScheduledThreadPool(1);
		}
		scheduler.scheduleAtFixedRate(cmd::execute, 0, period, MILLISECONDS); // execute la cmd passer dans le constructeur toutes les periodes millisecondes
//		cmd::execute, lambda expression qui dit appeler execute de cmd
	}
	
	public void stop(){
		scheduler.shutdownNow(); // ~scheduler = nuul
	}
}