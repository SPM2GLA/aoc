package AOC.AOC_V1.moteur;

import AOC.AOC_V1.commande.Commande;

public class MoteurImpl implements Moteur {

	private Boolean enMarche = false;
	private int tempo = 120;
	private int mesure = 4; // tout les n tempo
	private int compteurTempo = 0;
	private Commande commandeMesure = null;
	private Commande commandeTempo = null;
	private Commande commandeEnMarche = null;
	private Commande commandeMarquerTempo = null;
	private Commande commandeMarquerMesure = null;
	
	private Clock clock = new Clock(this::marquerTemps);
	public boolean getEnMarche() {
		return enMarche;
	}

	public void setEnMarche(boolean enmarche) {
		this.enMarche = enmarche;
		commandeEnMarche.execute();
		executeClock();
	}

	public int getTempo() {
		return tempo;
	}

	public void setTempo(int tempo) {
		this.tempo = tempo;
		commandeTempo.execute();
		executeClock();
	}

	public int getMesure() {
		return mesure;
	}

	public void setMesure(int mesure) {
		this.mesure = mesure;
		commandeMesure.execute();
		executeClock();
	}

	public void setCommandeTempo(Commande c) {
		commandeTempo = c;
	}

	public void setCommandeMesure(Commande c) {
		commandeMesure = c;
	}

	public void setCommandeEnMarche(Commande c) {
		commandeEnMarche = c;
	}

	public void setCommandeMarquerMesure(Commande c) {
		commandeMarquerMesure = c;
	}

	public void setCommandeMarquerTempo(Commande c) {
		commandeMarquerTempo = c;
	}

	private void executeClock(){
		if(enMarche)
			clock.start(mesure);
		else
			clock.stop();
	}
	
	private void marquerTemps(){
		commandeMarquerTempo.execute();
		compteurTempo = ++compteurTempo%mesure;
		if(compteurTempo == 0)
			commandeMarquerMesure.execute();
	}
	
}
